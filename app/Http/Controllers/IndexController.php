<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    //
    function home(){
        return view('tab.content');
    }

    function contact(){
        return view('tab.contact');
    }

    function page(){
        return view('tab.single-page');
    }
}
